import * as THREE from 'three';
import {FACES, verteces_for_faces, vertex_geom} from './geom.js';
import {BLOCKS, block_at, make_chunk, chunk_to_mesh} from './world.js';


let vec = THREE.Vector3;
let nvec = (x, y, z) => new THREE.Vector3(x, y, z);


const renderer = (function createRenderer() {
    let renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0x00bbcc, 1);
    document.body.style.margin = "0px";
    document.body.appendChild(renderer.domElement);

    renderer.domElement.requestPointerLock = renderer.domElement.requestPointerLock ||
        renderer.domElement.mozRequestPointerLock;
    return renderer;
})();


var chunk, cubes, camera, scene, skybox;
(function setUpScene() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

    cubes = [];

    let material = new THREE.MeshBasicMaterial({color: 0x000000, wireframe: true});

    chunk = make_chunk(128, 8);
    let meshes = chunk_to_mesh(chunk);

    cubes = new THREE.Mesh(meshes, material);
    cubes.matrixAutoUpdate = false;
    cubes.updateMatrix();
    scene.add(cubes);

    let sky_geom = new THREE.BoxGeometry();
    let sky_mat = new THREE.MeshBasicMaterial({color: 0x0066cc});
    sky_mat.side = THREE.DoubleSide;
    skybox = new THREE.Mesh(sky_geom, sky_mat);
    skybox.scale.set(1000, 1000, 1000);
    scene.add(skybox);


    camera.position.y = 9;
    camera.position.z = 15;
    camera.position.x = 15;
    scene.add(camera);
    console.log(scene);
})();


const keys = (function setUpControls() {
    let keys = { w: false, a: false, s: false, d: false, left: false, right: false, space: false, shift: false, down: false, up: false, f: false, r: false };

    let listener = function (keys, isDown) {
        return event => {
            switch(event.keyCode) {
                case 87:
                    keys.w = isDown;
                    break;
                case 83:
                    keys.s = isDown;
                    break;
                case 65:
                    keys.a = isDown;
                    break;
                case 68:
                    keys.d = isDown;
                    break;
                case 37:
                    keys.left = isDown;
                    break;
                case 39:
                    keys.right = isDown;
                    break;
                case 40:
                    keys.down = isDown;
                    break;
                case 38:
                    keys.up = isDown;
                    break;
                case 32:
                    keys.space = isDown;
                    break;
                case 16:
                    keys.shift = isDown;
                    break;
                case 70:
                    keys.f = isDown;
                    break;
                case 82:
                    keys.r = isDown;
                    break;
            }
        };
    }

    document.addEventListener("keydown", listener(keys, true));
    document.addEventListener("keyup", listener(keys, false));

    let camera_controlls = (e) => {
        let rot = {ver: 0, hor: 0};
        rot.hor = -e.movementX / 254;
        rot.ver = -e.movementY / 254;

        camera.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), rot.hor);
        camera.rotateOnAxis(new THREE.Vector3(1, 0, 0), rot.ver);
    };

    let lockChangeAlert = function() {
        if (document.pointerLockElement === renderer.domElement ||
            document.mozPointerLockElement === renderer.domElement)
            document.addEventListener("mousemove", camera_controlls, false);
        else
            document.removeEventListener("mousemove", camera_controlls, false);
    }

    renderer.domElement.onclick = renderer.domElement.requestPointerLock;
    document.addEventListener('pointerlockchange', lockChangeAlert, false);
    document.addEventListener('mozpointerlockchange', lockChangeAlert, false);

    return keys;
})();


let uniforms = {
    sampler: { value: null },
    ao_enabled_mix: { value: 1.0},
    sun_dir: { value: new THREE.Vector3(0.5, 0.75, 0.3).normalize() },
    ao_power: { value: 1.0 },
};

console.log(uniforms);

(async function setUpShaders() {
    {
        let req = await fetch("shaders/vertex.glsl");
        let vs = await req.text();
        req = await fetch("shaders/fragment.glsl");
        let fs = await req.text();

        let new_material = new THREE.ShaderMaterial( { wireframe: false, uniforms: uniforms, vertexShader: vs, fragmentShader: fs} );
        let tl = new THREE.TextureLoader();
        let texture = tl.load("brick.png");
        texture.magFilter = THREE.NearestFilter;
        texture.colorSpace = THREE.SRGBColorSpace;
        uniforms.sampler.value = texture;

        cubes.material = new_material;
    }
    {
        let req = await fetch("shaders/skybox_vertex.glsl");
        let vs = await req.text();
        req = await fetch("shaders/skybox_fragment.glsl");
        let fs = await req.text();

        let new_material = new THREE.ShaderMaterial( { uniforms: uniforms, vertexShader: vs, fragmentShader: fs} );
        new_material.side = THREE.DoubleSide;
        skybox.material = new_material;
    }
})();


let clock = new THREE.Clock();


function box_intersections(pos, margins) {
    for (let x = -1; x < 2; x += 2)
        for (let y = -1; y < 2; y += 2)
            for (let z = -1; z < 2; z += 2) {
                let p = pos.clone();
                let xi = (x + 1) / 2;
                let yi = (y + 1) / 2;
                let zi = (z + 1) / 2;
                let cm = nvec(margins.x[xi] * x, margins.y[yi] * y, margins.z[zi] * z);
                if (block_at(chunk, p.add(cm)) != BLOCKS.AIR)
                    return true;
            }
    return false;
}

// todo time do be varying. If you leave the tab open in bg for 1 min.
// it will fly through everything ignoring the geometry
function animate() {
    // time
    let dt = clock.getDelta();
    let time = clock.elapsedTime;

    let move = { x: 0, y: 0, z: 0 };
    move.y = keys.w - keys.s;
    move.x = keys.d - keys.a;
    move.z = keys.space - keys.shift;

    let fv = new THREE.Vector3(0, 0, 1);
    fv.applyQuaternion(camera.quaternion);
    let camera_angle = Math.atan2(fv.x, fv.z);

    let new_move = {
        x: Math.cos(camera_angle) * move.x - Math.sin(camera_angle) * move.y,
        y: Math.sin(camera_angle) * move.x + Math.cos(camera_angle) * move.y,
        z: move.z
    };

    let dz = 5 * -new_move.y * dt;
    let dx = 5 * new_move.x * dt;
    let dy = 5 * new_move.z * dt// + (-3) * dt;
    let margins = {x: [.25, .25], z: [.25, .25], y: [1.5, .25]};
    let old_cam = camera.position.clone();
    let cpos = old_cam.clone();
    if (!box_intersections(cpos.add(nvec(0, 0, dz)), margins))
        camera.position.z += dz;
    cpos = old_cam.clone();
    if (!box_intersections(cpos.add(nvec(dx, 0, 0)), margins))
        camera.position.x += dx;
    cpos = old_cam.clone();
    if (!box_intersections(cpos.add(nvec(0, dy, 0)), margins))
        camera.position.y += dy;

    let dist = camera.position.clone().sub(skybox.position);
    skybox.translateOnAxis(dist.clone().normalize(), dist.length());


    // todo stop looking too far up / down (>90 degrees)
    let rot = {ver: 0, hor: 0};
    rot.hor = (keys.left - keys.right) * dt * 1.4;
    rot.ver = (keys.up - keys.down) * dt * 1.4;

    camera.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), rot.hor);
    camera.rotateOnAxis(new THREE.Vector3(1, 0, 0), rot.ver);

    if (keys.f) {
        let dir = new THREE.Vector3();
        camera.getWorldDirection(dir);
        uniforms.sun_dir.value = dir;
    } else if (keys.r) {
        let dir = new THREE.Vector3();
        camera.getWorldDirection(dir);
        uniforms.sun_dir.value = dir.negate();
    }

    // render
    renderer.setRenderTarget(null);
    renderer.render(scene, camera);
    requestAnimationFrame(animate);


    //debug
    debug_.textContent = Math.floor(camera.position.x) + " " +Math.floor(camera.position.y) + " " + Math.floor(camera.position.z);
}

requestAnimationFrame(animate);
