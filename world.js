import * as THREE from 'three';
import {FACES, verteces_for_faces, vertex_geom} from './geom.js'



export const BLOCKS = (function() {
    return Object.freeze({
        AIR: 0,
        BRICK: 1
    });
})();



// todo generate chunk
// todo generate mesh from chunk
export const block_at = function (chunk, arg1, arg2, arg3) {
    let x, y, z;
    if (arg2 == null || arg3 == null) {
        let pos = arg1;
        x = pos.x;
        y = pos.y;
        z = pos.z;
    } else {
        x = arg1;
        y = arg2;
        z = arg3;
    }
    if (x < 0 || x >= chunk.length)
        return BLOCKS.AIR;
    if (z < 0 || z >= chunk[0].length)
        return BLOCKS.AIR;
    if (y < 0 || y >= chunk[0][0].length)
        return BLOCKS.AIR;
    return chunk[Math.floor(x)][Math.floor(z)][Math.floor(y)];
}


export const make_chunk = function (size, height) {
    let chunk = [];

    for(let x = 0; x < size; x++) {
        chunk[x] = [];
        for(let z = 0; z < size; z++) {
            chunk[x][z] = [];
            for(let y = 0; y < height; y++) {
                if ((y > (Math.floor(x/2)+Math.floor(z/2)) % 2) &&  y <= 1 + height / 2 + 4 * Math.sin( x / 4 ) * Math.sin( z / 4 )) {
                    chunk[x][z][y] = BLOCKS.BRICK;
                } else {
                    chunk[x][z][y] = BLOCKS.AIR;
                }
            }
        }
    }

    return chunk;
};

export const make_ao_test = function () {
    let chunk = [];

    let size = 8;
    let height = 8;

    for(let x = 0; x < size; x++) {
        chunk[x] = [];
        for(let z = 0; z < size; z++) {
            chunk[x][z] = [];
            for(let y = 0; y < height; y++) {
                let xe = x == 0 || x == size - 1;
                let ze = z == 0 || z == size - 1;
                let ye = y == 0 || y == height - 1;

                let xm = x == 1 || x == 3 || x == 4;
                let zm = z == 1 || z == 3 || z == 4;
                let ym = y == 1 || y == 3 || y == 4;

                if (xe && ye || ye && ze || ze && xe || xe && zm && ym || ze && xm && ym || ye && xm && zm)
                    chunk[x][z][y] = BLOCKS.AIR;
                else
                    chunk[x][z][y] = BLOCKS.BRICK;
            }
        }
    }

    return chunk;
};


// from https://0fps.net/2013/07/03/ambient-occlusion-for-minecraft-like-worlds/
let calc_occlusion = function (side1, side2, corner) {
    if (side1 != BLOCKS.AIR && side2 != BLOCKS.AIR)
        return 1.0 / 4.0;
    return (4.0 - (side1 != BLOCKS.AIR) - (side2 != BLOCKS.AIR) - (corner != BLOCKS.AIR)) / 4.0;
}


export const chunk_to_mesh = function (chunk) {
    let xl = chunk.length;
    let zl = chunk[0].length;
    let yl = chunk[0][0].length;

    let meshes = [];
    for (let x = 0; x < xl; x++) {
        meshes[x] = [];
        for (let z = 0; z < zl; z++) {
            meshes[x][z] = [];
            for (let y = 0; y < yl; y++) {
                meshes[x][z][y] = FACES.NONE;
            }
        }
    }


    let shadows = [];
    for (let x = 0; x < xl; x++) {
        shadows[x] = [];
        for (let z = 0; z < zl; z++) {
            shadows[x][z] = [];
            for (let y = 0; y < yl; y++) {
                shadows[x][z][y] = [];
                for (let side = 0; side < 6; side++) {
                    shadows[x][z][y][side] = [];
                }
            }
        }
    }


    let NTB = [
        [new THREE.Vector3(1, 0, 0), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 1)],
        [new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 1), new THREE.Vector3(1, 0, 0)],
        [new THREE.Vector3(0, 0, 1), new THREE.Vector3(-1, 0, 0), new THREE.Vector3(0, -1, 0)],
    ];
    let sides = [FACES.RIGHT, FACES.TOP, FACES.FRONT, FACES.LEFT, FACES.BOTTOM, FACES.BACK];
    for (let x = 0; x < xl; x++)
        for (let z = 0; z < zl; z++)
            for (let y = 0; y < yl; y++) {
                let pos = new THREE.Vector3(x, y, z);

                if (block_at(chunk, pos) != BLOCKS.BRICK)
                    continue;
                for (let side = 0; side < sides.length; side++) {
                    let pm = side > 2 ? -1 : 1;
                    let axis = side % 3;
                    let N = NTB[axis][0].clone().multiplyScalar(pm);
                    let T = NTB[axis][1].clone().multiplyScalar(pm);
                    let B = NTB[axis][2];

                    if (block_at(chunk, pos.clone().add(N)) != BLOCKS.AIR)
                        continue;
                    meshes[x][z][y] |= sides[side];
                    shadows[x][z][y][side][0] = calc_occlusion(block_at(chunk, pos.clone().add(N).sub(T) ), block_at(chunk, pos.clone().add(N).sub(B) ), block_at(chunk, pos.clone().add(N).sub(T).sub(B) ));
                    shadows[x][z][y][side][1] = calc_occlusion(block_at(chunk, pos.clone().add(N).add(T) ), block_at(chunk, pos.clone().add(N).sub(B) ), block_at(chunk, pos.clone().add(N).add(T).sub(B) ));
                    shadows[x][z][y][side][2] = calc_occlusion(block_at(chunk, pos.clone().add(N).add(T) ), block_at(chunk, pos.clone().add(N).add(B) ), block_at(chunk, pos.clone().add(N).add(T).add(B) ));
                    shadows[x][z][y][side][3] = calc_occlusion(block_at(chunk, pos.clone().add(N).sub(T) ), block_at(chunk, pos.clone().add(N).add(B) ), block_at(chunk, pos.clone().add(N).sub(T).add(B) ));
                }
            }

    let vertices = [];
    for (let x = 0; x < xl; x++)
        for (let z = 0; z < zl; z++)
            for (let y = 0; y < yl; y++) {
                if (chunk[x][z][y] == BLOCKS.BRICK) {
                    vertices.push(...verteces_for_faces(meshes[x][z][y], shadows[x][z][y], new THREE.Vector3(x + 0.5, y + 0.5, z + 0.5)));
                }
            }

    return vertex_geom(vertices);
}
