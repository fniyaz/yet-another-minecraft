precision highp float;

uniform vec3 sun_dir;

varying vec3 pos;


void main() {

    vec3 s_pos = normalize(pos);

    vec3 sky_color = vec3(0.0, 0.4, 1.0);
    vec3 sun_color = vec3(5.0);
    vec3 stars_color = vec3(1.0);
    vec3 ground_color = vec3(0.0, 0.0, 0.0);

    sky_color = mix(sky_color, sun_color, pow(max(0.0, dot(s_pos, sun_dir)), 2000.0));

    float star_freq = 10.0;
    float stars = abs(abs(s_pos.x)*star_freq - floor(abs(s_pos.x)*star_freq) - 0.5)*2.0;
    stars *= abs(abs(s_pos.y)*star_freq - floor(abs(s_pos.y)*star_freq) - 0.5)*2.0;
    ground_color = mix(ground_color, stars_color, pow(stars, 25.0));

    float horizon_fade = smoothstep(-0.15, 0.15, -s_pos.y);
    vec3 skybox = mix(sky_color, ground_color, horizon_fade);
    gl_FragColor = linearToOutputTexel(vec4(skybox, 1.0));
}

