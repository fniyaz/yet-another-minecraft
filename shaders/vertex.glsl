precision highp float;


attribute float occlusion;

uniform float time;

varying vec2 uvs;
varying float ambient_occlusion;
varying vec3 world_pos;
varying vec3 interpolated_normal;


void main() {
  vec4 pos = modelMatrix * vec4(position, 1.0);
  world_pos = pos.xyz / pos.w;
  uvs = uv;
  ambient_occlusion = occlusion;
  interpolated_normal = normal;


  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}

