precision highp float;


uniform float time;
uniform sampler2D sampler;
uniform float ao_enabled_mix;
uniform vec3 sun_dir;
uniform float ao_power;

varying vec2 uvs;
varying float ambient_occlusion;
varying vec3 world_pos;
varying vec3 interpolated_normal;


// https://www.shadertoy.com/view/cd3XWr
vec3 agxDefaultContrastApprox(vec3 x) {
  vec3 x2 = x * x;
  vec3 x4 = x2 * x2;

  return + 15.5     * x4 * x2
         - 40.14    * x4 * x
         + 31.96    * x4
         - 6.868    * x2 * x
         + 0.4298   * x2
         + 0.1191   * x
         - 0.00232;
}

vec3 agx(vec3 val) {
  const mat3 agx_mat = mat3(
    0.842479062253094, 0.0423282422610123, 0.0423756549057051,
    0.0784335999999992,  0.878468636469772,  0.0784336,
    0.0792237451477643, 0.0791661274605434, 0.879142973793104);

  const float min_ev = -12.47393f;
  const float max_ev = 4.026069f;

  // Input transform
  val = agx_mat * val;

  // Log2 space encoding
  val = clamp(log2(val), min_ev, max_ev);
  val = (val - min_ev) / (max_ev - min_ev);

  // Apply sigmoid function approximation
  val = agxDefaultContrastApprox(val);

  return val;
}


void main() {
    vec3 normal = normalize(interpolated_normal);

    vec3 albedo = pow(texture2D(sampler, uvs).rgb, vec3(1.0));
    albedo = vec3(0.5);

    vec3 ambient_color = mix(vec3(0.0, 0.4, 1.0), vec3(1.0), 0.75);
    float ao = mix(1.0, pow(ambient_occlusion, ao_power), ao_enabled_mix);
    vec3 ambient = 1.0 * ambient_color * ao;

    vec3 sun_color = vec3(1.0) * 10.0;
    vec3 diff = albedo / 3.1415;

    float r = 1.0;
    vec3 wi = sun_dir;
    vec3 wo = normalize(cameraPosition - world_pos);
    vec3 h = normalize(wo + wi);
    float f0 = 0.02;

    float a = max(0.00001, pow(r, 2.0));
    float D = a*a / (3.1415 * pow(pow(dot(normal, h), 2.0) * (a*a - 1.0) + 1.0, 2.0));
    float ndotl = max(0.0, dot(normal, wi));
    float ndotv = max(0.0, dot(normal, wo));

    float k = pow(r + 1.0, 2.0) / 8.0;
    float G11 = ndotv / (ndotv * (1.0 - k) + k);
    float G12 = ndotl / (ndotl * (1.0 - k) + k);
    float G = G11 * G12;

    float F = f0 + (1.0 - f0) * pow(1.0 - max(0.0, dot(h, wo)), 5.0);
    float bricks_spec_crutch = smoothstep(0.01, 0.05, dot(vec3(0.33), albedo));
    vec3 spec = vec3(1.0) * bricks_spec_crutch * D * G / 4.0 / ndotv / ndotl;
    if (dot(normal, wi) < 0.0)
        spec = vec3(0.0);

    vec3 color = sun_color * mix(diff, spec, F) * max(0.0, dot(sun_dir, normal)) + ambient * albedo;

    gl_FragColor = vec4(agx(color), 1.0);
    // gl_FragColor = vec4(vec3(bricks_spec_crutch), 1.0);
}

