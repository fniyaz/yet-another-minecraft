import * as THREE from 'three';


export const FACES = (function () {
    return Object.freeze({
        LEFT:   0b1,
        RIGHT:  0b10,
        TOP:    0b100,
        BOTTOM: 0b1000,
        BACK:   0b10000,
        FRONT:  0b100000,
        NONE:   0b0,
        ALL:    0b111111,
    });
})();


export const verteces_for_faces = function(faces, occl, pos) {
    let NTB = [
        [new THREE.Vector3(1, 0, 0), new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 1)],
        [new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 1), new THREE.Vector3(1, 0, 0)],
        [new THREE.Vector3(0, 0, 1), new THREE.Vector3(-1, 0, 0), new THREE.Vector3(0, -1, 0)],
    ];
    let sides = [FACES.RIGHT, FACES.TOP, FACES.FRONT, FACES.LEFT, FACES.BOTTOM, FACES.BACK];

    let res = [];
    for (let side = 0; side < sides.length; side++) {
        if (!(faces & sides[side]))
            continue;

        let pm = side > 2 ? -1 : 1;
        let axis = side % 3;
        let N = NTB[axis][0].clone().multiplyScalar(pm);
        let T = NTB[axis][1].clone().multiplyScalar(pm);
        let B = NTB[axis][2];

        if (occl[side][0] + occl[side][2] < occl[side][1] + occl[side][3]) {
            res.push({ pos: pos.clone().add( N.clone().sub(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [0, 0], occlusion: occl[side][0] });
            res.push({ pos: pos.clone().add( N.clone().add(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [1, 0], occlusion: occl[side][1] });
            res.push({ pos: pos.clone().add( N.clone().sub(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [0, 1], occlusion: occl[side][3] });

            res.push({ pos: pos.clone().add( N.clone().sub(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [0, 1], occlusion: occl[side][3] });
            res.push({ pos: pos.clone().add( N.clone().add(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [1, 0], occlusion: occl[side][1] });
            res.push({ pos: pos.clone().add( N.clone().add(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [1, 1], occlusion: occl[side][2] });
        } else {
            res.push({ pos: pos.clone().add( N.clone().sub(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [0, 0], occlusion: occl[side][0] });
            res.push({ pos: pos.clone().add( N.clone().add(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [1, 0], occlusion: occl[side][1] });
            res.push({ pos: pos.clone().add( N.clone().add(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [1, 1], occlusion: occl[side][2] });

            res.push({ pos: pos.clone().add( N.clone().sub(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [0, 1], occlusion: occl[side][3] });
            res.push({ pos: pos.clone().add( N.clone().sub(T).sub(B).multiplyScalar(0.5) ), norm: N, uv: [0, 0], occlusion: occl[side][0] });
            res.push({ pos: pos.clone().add( N.clone().add(T).add(B).multiplyScalar(0.5) ), norm: N, uv: [1, 1], occlusion: occl[side][2] });
        }
    }

    return res;
};

export const vertex_geom = (function (vertices) {
    let positions = [];
    let normals = [];
    let uvs = [];
    let occlusion = [];
    for (const vertex of vertices) {
        positions.push(...vertex.pos);
        normals.push(...vertex.norm);
        uvs.push(...vertex.uv);
        occlusion.push(vertex.occlusion);
    }
    let geometry = new THREE.BufferGeometry();
    let positionNumComponents = 3;
    let normalNumComponents = 3;
    let uvNumComponents = 2;
    let occlusionNumComponents = 1;
    geometry.setAttribute(
        'position',
        new THREE.BufferAttribute(new Float32Array(positions), positionNumComponents));
    geometry.setAttribute(
        'normal',
        new THREE.BufferAttribute(new Float32Array(normals), normalNumComponents));
    geometry.setAttribute(
        'uv',
        new THREE.BufferAttribute(new Float32Array(uvs), uvNumComponents));
    geometry.setAttribute(
        'occlusion',
        new THREE.BufferAttribute(new Float32Array(occlusion), occlusionNumComponents));

    return geometry;
});

